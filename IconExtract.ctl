VERSION 5.00
Begin VB.UserControl IconExtract 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000005&
   ClientHeight    =   240
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   240
   ClipControls    =   0   'False
   ScaleHeight     =   240
   ScaleWidth      =   240
End
Attribute VB_Name = "IconExtract"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const DI_MASK = &H1
Const DI_IMAGE = &H2
Const DI_NORMAL = DI_MASK Or DI_IMAGE

Private Declare Function ExtractIconEx Lib "shell32.dll" Alias "ExtractIconExA" (ByVal lpszFile As String, ByVal nIconIndex As Long, phiconLarge As Long, phiconSmall As Long, ByVal nIcons As Long) As Long
Private Declare Function DrawIconEx Lib "user32" (ByVal hDC As Long, ByVal xLeft As Long, ByVal yTop As Long, ByVal hIcon As Long, ByVal cxWidth As Long, ByVal cyHeight As Long, ByVal istepIfAniCur As Long, ByVal hbrFlickerFreeDraw As Long, ByVal diFlags As Long) As Long
Private Declare Function DestroyIcon Lib "user32" (ByVal hIcon As Long) As Long

Private sIconFile As String
Private iRetCode As Integer
Private iIconSize As Integer

Public Property Let SourceFile(ByVal sFilePath As String)
    Dim hIconSmall, hIconLarge As Long
    Dim iIconCount As Integer
    sIconFile = sFilePath
    iIconCount = ExtractIconEx(sFilePath, 0, hIconLarge, hIconSmall, 1)
    If iIconCount > 0 Then
        If iIconSize = 16 Then
            iRetCode = DrawIconEx(UserControl.hDC, 0, 0, hIconSmall, iIconSize, iIconSize, 0, 0, DI_NORMAL)
        Else
            iRetCode = DrawIconEx(UserControl.hDC, 0, 0, hIconLarge, iIconSize, iIconSize, 0, 0, DI_NORMAL)
        End If
        UserControl.Refresh
        DestroyIcon hIconSmall
        DestroyIcon hIconLarge
    End If
End Property

Public Property Get SourceFile() As String
    SourceFile = sIconFile
End Property

Public Property Get ReturnCode() As Integer
    ReturnCode = iRetCode
End Property

Public Property Let IconSize(ByVal iSize As Integer)
    iIconSize = iSize
    UserControl.Height = iSize * 15
    UserControl.Width = iSize * 15
End Property

Private Sub UserControl_Initialize()
    iIconSize = 16
End Sub
